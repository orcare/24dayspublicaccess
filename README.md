# README #

### What is this repository for? ###

This project demonstrates how to get a Membership based Umbraco site up and running quickly using Umbraco Level 1 skills.  This project accompanies my article for [24Days.in/umbraco](http://24days.in/umbraco/)

### Getting started ###

Run the site up on a local IIS instance, I've included the SQL CE database for ease of use.

Umbraco: login = admin / admin1234

Frontend Member: TestMember1 / abcd1234

### Contribution guidelines ###

Feel free to contribute to the code - making any suggests as you see fit.

### Who do I talk to? ###

* Paul Marden [@orcare](http://twitter.com/orcare)